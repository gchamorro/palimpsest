#!/bin/bash
# License: GPL 
# Author: Guillermo Chamorro <guillechamorro _at_ gmail com>
# Description: The start of the palimpsest program

existing="$1"

persistence() {
  local iso="$1"
  local iso_to_array=("$iso")
  local iso_name="${iso##*/}"
  create_persistence_file "${iso}"
  if [[ "${existing}" -eq 1 ]]; then
    install_isos "pe" "${iso_to_array}"
  else
    install_isos "p" "${iso_to_array}"
  fi
}

persistence_exists() {
  if [[ -f "${mount_tmp_dir}"/casper-rw ]]; then
    whiptail \
    --backtitle "${backtitle}" \
    --title "Persistencia" \
    --msgbox "Ya existe una instalación con persistencia." \
    8 40
    local response="$?"
    case "${response}" in
      0|1|255|*) start
    esac
  fi
}

create_persistence_file() {
  local stop=0
  local iso=("$1")
  while [[ "${stop}" -eq 0 ]]; do
    local size=$(whiptail \
      --backtitle "${backtitle}" \
      --title "Archivo persistente" \
      --inputbox "Ingrese el tamaño del archivo de persistencia en MB.\nMínimo: 200MB\nMáximo: 3500MB" \
      20 50 "200" \
      2>&1 > /dev/tty)
    case "${response}" in
    "") start
    esac
    if [[ ! "${size}" =~ ^[0-9]+$ ]] || [[ (( "${size}" -lt 200 || "${size}" -gt 3500 )) ]]; then
      whiptail --backtitle "${backtitle}" \
        --title "Archivo persistente" \
        --msgbox "El valor ingresado no es correcto." \
        8 40
    else
      space "${size}" "${iso}"
      messages "Creando el archivo de persistencia, espere un momento por favor..."
      dd if=/dev/zero of="${mount_tmp_dir}"/casper-rw bs=1M count="${size}" status=progress || ( warning "No se ha podido crear el archivo de persistencia" && start )
      mkfs.ext4 -L casper-rw -F "${mount_tmp_dir}"/casper-rw ||  ( warning "No se ha podido crear el archivo de persistencia" && rm "${mount_tmp_dir}"/casper-rw && start )
      return
    fi
  done
}

show_isos() {
  local tmp_file="/tmp/palimpsest_tmp_file"
  local isos_found=("$@")
  local list=()
  if [[ -z "${isos_found}" ]]; then
    whiptail \
    --backtitle "${backtitle}" \
    --title "Selección de imágenes" \
    --msgbox "¡No se ha encontrado ninguna imagen ISO!" \
    10 50
    local response="$?"
    case "${response}" in
      0|1|255) start
    esac
  fi
  echo
  for i in "${isos_found[@]}"; do 
    list+=("$i" "" off)
  done
  local list_height="${#isos_found[@]}"
  local height=20
  [[ "${list_height}" -gt 15 ]] && height=25
  while :; do 
    whiptail \
    --backtitle "${backtitle}" \
    --title "Seleccione imágenes" \
    --radiolist "" \
    $(stty size) "${list_height}" \
    "${list[@]}" \
    2> "${tmp_file}"
    local response=$?
    read -a selected_iso < "${tmp_file}"
    [[ ! -z "${selected_iso}" ]] && persistence "${selected_iso}" && return
    case "${response}" in
    "") whiptail \
        --backtitle "${backtitle}" \
        --title "Selección de imágenes" \
        --msgbox "¡No ha selecciona do ninguna imagen ISO!" \
        10 50 \
        2>&1 > /dev/tty;;
      1|255) start;;
    esac
  done
}

scan_isos() {
  local isos_search=()
  local isos_found=()
  local installed_systems=()
  local supported_isos=()
  if [[ -f "${palimpsest_is_file}" ]]; then
    mapfile -t installed_systems < "${palimpsest_is_file}"
    for iso in "${installed_systems[@]}"; do 
      messages "G#${iso}" "ya está instalado."
    done
  fi
  if [[ "${existing}" -eq 1 ]]; then
    local dirs=("${mount_tmp_dir}/isos")
  else
    local dirs=("/mnt" "/media" "/home")
  fi
  messages "Buscando imágenes ISO en sus dispositivos, espere un momento por favor..."
  for dir in "${dirs[@]}" ; do
    isos_search+=("$(find "${dir}" -not -path '*/\.*' -type f -name "*.iso")")
  done
  readarray -t array <<< $(printf '%s\n' "${isos_search[@]}")
  while IFS= read -r prefix; do
    for iso in "${array[@]}"; do
      if [[ "$(basename "$iso")" =~ "$prefix".*$ ]]; then
        messages "Imagen" "G#$iso" "encontrada."
        matched_isos+=("$iso")
      fi
    done
  done < supported_systems_persistence
  for iso in "${matched_isos[@]}" ; do
    if [[ "${existing}" -eq 1 ]]; then
      [[ "${installed_systems[@]}" =~ "$(basename "$iso")" ]] && isos_found+=("$iso")
    else
      [[ ! "${installed_systems[@]}" =~ "$(basename "$iso")" ]] && isos_found+=("$iso")
    fi
  done
  show_isos "${isos_found[@]}"
}

main() {
  persistence_exists
  scan_isos
}

main
